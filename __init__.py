# -*- coding: utf-8 -*-

def pre_version_check(cr):
    from odoo.service import common
    from odoo.exceptions import UserError

    version_info = common.exp_version()
    server_serie = version_info.get('server_serie')
    if server_serie != '11.0':
        raise UserError('Module support Odoo series 11.0 found {}'.format(server_serie))
    return True
