# -*- coding: utf-8 -*-

{
    'name': 'POS Remove Orderline',
    'version': '1.0',
    'summary': 'POS remove orderline using delete key',
    'description': """
Remove orderline using delete key
=================================
    """,
    'author': 'MP Technolabs',
    'website': 'http://www.mptechnolabs.com',
    'license': 'AGPL-3',
    'category': 'Point of Sale',
    'depends': ['point_of_sale'],
    'data': [
        'views/templates.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': False,
    'pre_init_hook': 'pre_version_check',
    'currency': 'EUR',
    'price': 10.00,
}
